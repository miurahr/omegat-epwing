# Security Policy

## Supported Versions

| Version   | Status            | OmegaT    | Java  |
|-----------|-------------------|-----------|-------|
| 3.0.x     | Stable            | 6.0 -     | 11,17 |
| 2.3.x     | Security fix only | 4.3 - 5.8 | 8,11  |
| < 2.3     | not supported     |           |       |

## Reporting a Vulnerability

Please disclose security vulnerabilities privately at miurahr@linux.com in English or Japanese.
