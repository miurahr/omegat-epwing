import org.gradle.crypto.checksum.Checksum

plugins {
    java
    groovy
    checkstyle
    jacoco
    distribution
    signing
    id("org.omegat.gradle") version "1.5.9"
    id("org.gradle.crypto.checksum") version "1.4.0"
    id("com.github.spotbugs") version "5.0.9"
    id("com.diffplug.spotless") version "6.8.0"
    id("com.palantir.git-version") version "0.13.0" apply false
}

// we handle cases without .git directory
val dotgit = project.file(".git")
if (dotgit.exists()) {
    // calculate version string from git tag, hash and commit distance
    apply(plugin = "com.palantir.git-version")
    val versionDetails: groovy.lang.Closure<com.palantir.gradle.gitversion.VersionDetails> by extra
    val details = versionDetails()
    val baseVersion = details.lastTag.substring(1)
    version = if (details.isCleanTag) baseVersion
        else baseVersion + "-" + details.commitDistance + "-" + details.gitHash + "-SNAPSHOT"

    val signKey = listOf("signingKey", "signing.keyId", "signing.gnupg.keyName").find {project.hasProperty(it)}
    tasks.withType<Sign> {
        onlyIf { details.isCleanTag && (signKey != null) }
    }

    signing {
        when (signKey) {
            "signingKey" -> {
                val signingKey: String? by project
                val signingPassword: String? by project
                useInMemoryPgpKeys(signingKey, signingPassword)
            }
            "signing.keyId" -> {/* do nothing */}
            "signing.gnupg.keyName" -> {
                useGpgCmd()
            }
        }
        sign(tasks.distZip.get())
        sign(tasks.jar.get())
    }
} else {
    println("Read version property from gradle.properties.")
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
}

omegat {
    version = "5.7.1"
    pluginClass = "tokyo.northside.omegat.epwing.OmegatEpwingDictionary"
    packIntoJarFileFilter = {it.exclude("META-INF/**/*", "module-info.class", "kotlin/**/*")}
}

repositories {
    mavenCentral()
}

dependencies {
    packIntoJar("tokyo.northside:eb4j:3.0.1")
    packIntoJar("commons-lang:commons-lang:2.6")
    // should not bundle apache-commons
    testImplementation("commons-io:commons-io:2.11.0")
    testImplementation("commons-lang:commons-lang:2.6")
    testImplementation("org.codehaus.groovy:groovy-all:3.0.11")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.1")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.9.1")
    testImplementation("com.github.spotbugs:spotbugs-annotations:4.7.1")
}

jacoco {
    toolVersion="0.8.6"
}

tasks.jacocoTestReport {
    dependsOn(tasks.test) // tests are required to run before generating the report
    reports {
        xml.required.set(false)
        html.required.set(true)
    }
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

distributions {
    main {
        contents {
            from(tasks["jar"], "README.md", "CHANGELOG.md", "COPYING")
        }
    }
}

spotless {
    java {
        target(listOf("src/*/java/**/*.java"))
        removeUnusedImports()
        palantirJavaFormat()
        importOrder("org.omegat", "io.github.eb4j", "java", "javax", "", "\\#")
    }
}

tasks.register<Checksum>("createChecksums") {
    dependsOn(tasks.distZip)
    inputFiles.setFrom(listOf(tasks.jar.get(), tasks.distZip.get()))
    outputDirectory.set(layout.buildDirectory.dir("distributions"))
    checksumAlgorithm.set(Checksum.Algorithm.SHA512)
    appendFileNameToChecksum.set(true)
}
