/*
 * Copyright (C) 2022 Hiroshi Miura.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tokyo.northside.omegat.epwing;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Hiroshi Miura
 * @param <K> cache key
 * @param <V> cache value
 */
public class SimpleLRUCache<K, V> extends LinkedHashMap<K, V> implements Serializable {

    private static final long serialVersionUID = 2L;

    static final float LOAD_FACTOR = 0.75f;

    private final int maximumCacheSize;

    public SimpleLRUCache(final int initialCapacity, final int maxCacheSize) {
        super(initialCapacity, LOAD_FACTOR, true);
        this.maximumCacheSize = maxCacheSize;
    }

    @Override
    protected boolean removeEldestEntry(final Map.Entry<K, V> eldest) {
        return size() > maximumCacheSize;
    }
}
