/*
 * Copyright (C) 2022 Hiroshi Miura.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tokyo.northside.omegat.epwing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SimpleLRUCacheTest {

    @Test
    void removeEldestEntry() {
        SimpleLRUCache<Integer, String> cache = new SimpleLRUCache<>(2, 4);
        cache.put(0, "0");
        cache.put(1, "1");
        Assertions.assertEquals(2, cache.size());
        Assertions.assertEquals("0", cache.get(0));
        cache.put(2, "2");
        cache.put(3, "3");
        Assertions.assertEquals(4, cache.size());
        cache.put(4, "4");
        Assertions.assertEquals(4, cache.size());
        Assertions.assertEquals("0", cache.get(0));
        Assertions.assertNull(cache.get(1));
        Assertions.assertEquals("4", cache.get(4));
    }
}
